<?php
namespace Saamag\Pincode\Block\Adminhtml;
class Pincode extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_pincode';/*block grid.php directory*/
        $this->_blockGroup = 'Saamag_Pincode';
        $this->_headerText = __('Pincode');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		
    }
}
