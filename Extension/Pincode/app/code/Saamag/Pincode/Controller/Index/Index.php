<?php
namespace Saamag\Pincode\Controller\Index;
class Index extends \Magento\Framework\App\Action\Action
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context)
    {
        return parent::__construct($context);
    }

    public function execute()
    {
        $zipcode =  $_POST["zipcode"];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('pincode_pincode'); //gives table name with prefix
		 
		//Select Data from table
		$sql = "Select * FROM " . $tableName." Where pincode=".$zipcode."";
		$result = $connection->fetchAll($sql);
		if(!empty($result))
		{
			$city = $result['0']['city'];
			$state = $result['0']['state'];
			$ar = array(
				"city" => $city,
				"state" => $state
			);
			echo json_encode($ar);
		}
		else{
			$city = 'no';
			$state = 'no';
			$ar = array(
				"city" => $city,
				"state" => $state
			);
			echo json_encode($ar);
		}
        exit;
    }
}
