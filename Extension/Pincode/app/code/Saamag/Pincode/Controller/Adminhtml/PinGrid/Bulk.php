<?php
namespace Saamag\Pincode\Controller\Adminhtml\PinGrid;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


class Bulk extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
		
		 //die('test admin view');
		$this->resultPage = $this->resultPageFactory->create();  
		$this->resultPage->setActiveMenu('Saamag_PinGrid::pincode');
		$this->resultPage ->getConfig()->getTitle()->set((__('Bulk Pincode')));
		return $this->resultPage; 
    }
}
