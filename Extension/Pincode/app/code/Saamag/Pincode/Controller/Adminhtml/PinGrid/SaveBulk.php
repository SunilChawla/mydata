<?php

namespace Saamag\Pincode\Controller\Adminhtml\PinGrid;
use Magento\Backend\App\Action;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class SaveBulk extends \Magento\Backend\App\Action
{
    /* @param Action\Context $context
     */
   /*  protected $_fileUploaderFactory;
    public function __construct(Action\Context $context,\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,StoreManagerInterface $storeManager,\Magento\Framework\Filesystem $filesystem)
    {
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        parent::__construct($context);
    } */
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != '')
		{
			try
			{
				$uploader = $this->_objectManager->create('\Magento\MediaStorage\Model\File\Uploader',['fileId'=>'file']);
				$uploader->setAllowedExtensions(array('csv'));
				$uploader->setAllowRenameFiles(true);
				$uploader->setFilesDispersion(true);
				$mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(DirectoryList::MEDIA);
				$result = $uploader->save($mediaDirectory->getAbsolutePath('pincode/sheets/'));
				$filepath = $result['path'].$result['file'];
				$i = 1;
				$errors= '';
				if(($handle = fopen("$filepath", "r")) !== FALSE)
				{
					while(($data = fgetcsv($handle, 10000, ",")) !== FALSE)
					{             
						if($i>1 && $i<10001 && count($data)>0){
							try 
							{
								$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
								$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
								$connection = $resource->getConnection();
								$tableName = $resource->getTableName('pincode_pincode');
								$sql = "SELECT * FROM " . $tableName . " where pincode='".$data[0]."'";
								$meet = $connection->fetchAll($sql);
								if(!$meet){	
									$sql = "Insert Into " . $tableName . " (pincode, status, tat, city,state) Values ('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]')";
									$connection->query($sql);
								}
								else{
									$sqlupdate ="UPDATE " . $tableName . " SET `status` = '$data[1]',`tat` = '$data[2]',`city` = '$data[3]',`state` = '$data[4]' WHERE `pincode` = '$data[0]'"; 
									$connection->query($sqlupdate);
								}
								$this->messageManager->addSuccess(__('Pincodes updated successfully.'));

							} catch (Exception $e) {
								$this->messageManager->addError("Error at line ".$i.' '.$e->getMessage());
							}
						
						} 
						$i++;
					}
				}
			} 
			catch (Exception $e) {
				 $this->messageManager->addException($e->getMessage());
			}
		}
		else{
			$this->messageManager->addException("file not uploaded");
		}
		$this->_redirect('*/*/bulk');
    }
}