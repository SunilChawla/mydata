<?php
/**
 * Copyright © 2015 Saamag . All rights reserved.
 */
namespace Saamag\Pincode\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(\Magento\Framework\App\Helper\Context $context
	) {
		parent::__construct($context);
	}
}