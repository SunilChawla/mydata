<?php
/**
 * Copyright © 2015 Saamag. All rights reserved.
 */
namespace Saamag\Pincode\Model\ResourceModel;

/**
 * Pincode resource
 */
class Pincode extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('pincode_pincode', 'id');
    }

  
}
