<?php
  
namespace Kan\OrderTab\Model;
  
use Magento\Framework\Model\AbstractModel;
  
class Sample extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Kan\OrderTab\Model\ResourceModel\Sample');
    }
}