<?php
  
namespace Kan\OrderTab\Model\ResourceModel\Sample;
  
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
  
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Kan\OrderTab\Model\Sample',
            'Kan\OrderTab\Model\ResourceModel\Sample'
        );
    }
}