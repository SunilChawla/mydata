<?php
namespace Kan\OrderTab\Ui\Component\Listing\Column;
  
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
  
class Calling extends Column
{
    /**
     * 
     * @param ContextInterface   $context           
     * @param UiComponentFactory $uiComponentFactory   
     * @param array              $components        
     * @param array              $data              
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
  
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
	   if (isset($dataSource['data']['items'])) {
		   $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
			$connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
            foreach ($dataSource['data']['items'] as & $item) 
			{
				$remarks = '';
				$orderid = $item['entity_id'];
				$result = $connection->fetchAll("SELECT status FROM cc_remarks where order_id=".$orderid." order by remarks_id asc");
				if($result)
				{
					foreach($result as $data){
						
						$remarks .= $data['status'].' ';
					}
					
				}
                $item[$this->getData('name')] = $remarks;
            }
        } 
        return $dataSource;
    }
}