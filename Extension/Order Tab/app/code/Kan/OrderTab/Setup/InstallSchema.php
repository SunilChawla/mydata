<?php
/**
* Copyright © 2016 Magento. All rights reserved.
* See COPYING.txt for license details.
*/

namespace Kan\OrderTab\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
    * {@inheritdoc}
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
          * Create table 'cc_remarks'
          */
           $installer = $setup;
			$installer->startSetup();
		  $table = $setup->getConnection()
              ->newTable($setup->getTable('cc_remarks'))
              ->addColumn(
                  'remarks_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                  'Remark ID'
              )
			   ->addColumn(
                  'order_id',
                  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                  null,
                  ['nullable' => false],
                  'Order ID'
              )
			  ->addColumn(
                  'orderincreamentid',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  25,
                  ['nullable' => false, 'default' => ''],
                    'Order Increment Id'
              )
			  ->addColumn(
                  'comment',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  255,
                  ['nullable' => false, 'default' => ''],
                    'Comment'
              )
              ->addColumn(
                  'status',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  255,
                  ['nullable' => false, 'default' => ''],
                    'Status'
              )->setComment("CC Remarks table");
          $setup->getConnection()->createTable($table);
      }
}