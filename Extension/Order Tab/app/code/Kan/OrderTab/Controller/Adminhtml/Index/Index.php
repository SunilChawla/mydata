<?php
  
namespace Kan\OrderTab\Controller\Adminhtml\Index;
  
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Kan\OrderTab\Model\SampleFactory;
  
class Index extends Action
{
    /**
     * @var \Kan\OrderTab\Model\SampleFactory
     */
    protected $_modelSampleFactory;
  
    /**
     * @param Context $context
     * @param SampleFactory $modelSampleFactory
     */
    public function __construct(
        Context $context,
        SampleFactory $modelSampleFactory
    ) {
        parent::__construct($context);
        $this->_modelSampleFactory = $modelSampleFactory;
    }
  
    public function execute()
    {
        $sampleModel = $this->_modelSampleFactory->create();
  
        // Load the item with ID is 1
        $item = $sampleModel->load(1);
        var_dump($item->getData());
  
        // Get sample collection
        $sampleCollection = $sampleModel->getCollection();
        // Load all data of collection
        var_dump($sampleCollection->getData());
    }
}