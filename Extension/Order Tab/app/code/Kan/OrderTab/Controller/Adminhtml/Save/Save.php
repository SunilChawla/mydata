<?php
namespace Kan\OrderTab\Controller\Adminhtml\Save;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Filesystem;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
	public function execute()
    {
		$data = $this->getRequest()->getParams();
		/* echo '<pre>';
		print_r($data);
		echo '</pre>'; die('14'); */
		$model = $this->_objectManager->create('Kan\OrderTab\Model\Sample');
		$model->setData($data);
		try {
			$model->save();
			$this->messageManager->addSuccess(__('Details Has been Saved.'));
			$this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
			$this->_redirect('sales/order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
			return;
		} catch (\Magento\Framework\Model\Exception $e) {
			$this->messageManager->addError($e->getMessage());
		} catch (\RuntimeException $e) {
			$this->messageManager->addError($e->getMessage());
		} catch (\Exception $e) {
			$this->messageManager->addException($e, __('Something went wrong while details.'));
		}

		$this->_getSession()->setFormData($data);
		$this->_redirect('sales/order/view', array('order_id' => $this->getRequest()->getParam('order_id')));
		return;
    }
}
